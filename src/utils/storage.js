export const storageSave = (key, value) => {
   
   if(!key || typeof key !== 'string') {
    throw new Error('staorageSave: No storage key provided')
   }

   if(!value) {
    throw new Error('staorageSave: No storage value provided' + key)
   }
    sessionStorage.setItem(key, JSON.stringify(value))
} 

export const storageRead = key => {
    
    if(!key || typeof key !== 'string') {
        throw new Error('staorageRead: No storage key provided')
    }

    const data = sessionStorage.getItem(key)

    if(data) {
        return JSON.parse(data)
    }
    return null
}

export const storageDelete = key => {
    
    if(!key || typeof key !== 'string') {
        throw new Error('staorageDelete: No storage key provided')
    }
    sessionStorage.removeItem(key)
}