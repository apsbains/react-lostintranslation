import { NavLink, Link } from "react-router-dom"
import { useUser } from '../../context/UserContext'

const Navbar = () => {

    const { user } = useUser()
    return (
        <nav>
           <div className='container'>
                <div className='row'>
                    <div className='col-md-1'>
                    
                    { user!== null && 
                        <img src="./Logo.png" alt="" width="40" align='left'/>
                    }
                    </div>
                    <div className='col-md-4'>
                        <span>Lost in Translations</span>
                    </div>
                    <div className='col-md-5'>
                        
                    </div>
                    <div className='col-md-2'>
                        <span>{ user!== null && user.username }</span>
                    </div>

                </div>
                
               
                { user!== null &&
                
                <div className='row'>
                    
                    <div className='col-md-1 offset-md-4'>
                        <Link as={ Link } to="/translation">Translation</Link>
                    </div>
                
                    <div className='col-md-1 offset-md-4'>
                        <NavLink exact={ true } to="/profile">Profile</NavLink>
                    </div>
                </div>                              
                }
                <div className='row'>
                    <div className='col-12'>
                        <span id='margin-span'></span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-12'>
                        <span id='margin-span'></span>
                    </div>
                </div>  
            </div>
        </nav>
    )
}
export default Navbar