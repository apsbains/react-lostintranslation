import { Link } from "react-router-dom"
import { useUser } from "../../context/UserContext"
import { storageDelete, storageSave } from "../../utils/storage"
import { STORAGE_KEY_USER } from '../../const/storageKeys'
import { translationClearHistory } from "../../api/translation"

const ProfileActions = ({ logout }) => {
    
    const { user, setUser } = useUser()

    const handleLogoutClick = () => {
        if(window.confirm('Are you sure')) {
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
            //logout()
        }
    }
    
    const handleClearHistoryClick = async () => {
        if(!window.confirm('Are you sure? \n This cannot be undone')) {
          return
            //logout()
        }
        const [ clearError ] = await translationClearHistory(user.id)
        if(clearError !== null ) {
            return
        }

        const updatedUser = { 
            ...user, 
            translations: []
        } 

        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
    }
    return (
        <ul>            
            <li><button onClick={ handleClearHistoryClick }>Clear History</button></li>
            <li><button onClick={ handleLogoutClick }>Logout</button></li>
        </ul>
    )
}
export default ProfileActions