import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"

const ProfileTranslationHistory = ({ translations }) => {
    const translationsList = translations.map(
        (translation, index) => index<10 ? 
        <ProfileTranslationHistoryItem key={ index + '-' + translation } item={ translation }/> 
        : <div></div>) 
        
        return (
        <section>
            <h4>Your Translations History first ten </h4>
            <ul>
                { translationsList }
            </ul>
        </section>
        

    )
}
export default ProfileTranslationHistory