import { useState, useEffect } from 'react'
import { useForm} from 'react-hook-form'
import {loginUser } from '../../api/user'
import { storageSave } from '../../utils/storage'
import { useNavigate } from 'react-router-dom'
import { useUser } from '../../context/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'


const usernameConfig = {
    required: true,
    minLength: 3
}

const LoginForm = () => {
    
    //Hooks
    const { register, handleSubmit, formState: { errors } } = useForm()
    const { user, setUser } = useUser()
    const navigate = useNavigate()

    //Local state
    const [ loading, setLoading ] = useState(false)
    const [ apiError, setApiError ] = useState(null)

    //Side effects
    useEffect(() => {
        if(user !== null) {
            navigate('/translation')
        }
        
    }, [ user, navigate ])

    //Event handler
    const onSubmit = async ({ username }) => {
        setLoading(true)
        const [error, userResponse] = await loginUser(username)
        
        if(error !== null) {
            setApiError(error)
        }
        if(userResponse !== null){
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
        setLoading(false)
        
    }
    
    // Render functions
    const errorMessage = (() => {
        
        if(!errors.username) {
            return null
        }

        if(errors.username.type === 'required') {
            return <span>Username is required</span>
        }

        if(errors.username.type === 'minLength'){
            return <span>Username is too short(min. 3)</span>
        }
    })() 

    return(
        
            <div id='login-container'>
                <br/>
                <br/>
                <br/>
                <div className='row'> 
                    <div className= 'col-2'>
                    </div>
                   <div className= 'col-8'>
                        <form onSubmit={ handleSubmit(onSubmit) } >      
                            <div id='username-input' class="input-group mb-3">
                            <span className="material-icons-outlined">keyboard_alt</span>
                                
                                <input className='form-control' id='username-input'
                                type="text"
                                placeholder="whats your name?" 
                                {...register("username", usernameConfig)} /> 
                            
                                
                                <button id='round-button' 
                                className='btn btn-primary btn-rounded btn-icon' 
                                type="submit" width='20' disabled={ loading }>
                                    <span className="material-icons-outlined">arrow_forward</span>
                                </button>
                                <br/>
                                { errorMessage }              
                                
                            
                                { loading && <p>Logging in...</p> }
                                { apiError && <p>{ apiError }</p>}
                            </div>    
                        </form>
                    </div>
                </div>    
                <br/>
                <div className='row' id='purple'>
                 <div className='col-md-12'><label id='purple'>t</label></div>                 
                </div>               
            </div>
             
    )
}
export default LoginForm