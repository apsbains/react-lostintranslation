import { useForm } from "react-hook-form"

const translationConfig = {
    required: true,
    maxLength: 40
}

const TranslationForm = ({ onTranslation }) => {
    
    const { register, handleSubmit, formState: { errors } } = useForm()
    
    const onSubmit = ({ translationNotes }) => { onTranslation(translationNotes) }

    // Render functions
    const errorMessage = (() => {
        
        if(!errors.translationNotes) {
            return null
        }

        if(errors.translationNotes.type === 'required') {
            return <span>Translation is required</span>
        }

        if(errors.translationNotes.type >= 'maxLength'){
            return <span>Translation is too long(max. 40)</span>
        }
    })() 

    return (
        <div className='login-container'>
        <div className="row">
            <div className='col'>
                <form onSubmit={ handleSubmit(onSubmit)}>
                    <div id='username-input' class="input-group mb-3">
                    <span className="material-icons-outlined">keyboard_alt</span>
                        <input id='username-input' class='form-control' type="text" 
                        { ...register('translationNotes', translationConfig)} 
                        placeholder="type translation"/>
                    
                        <button id='round-button' 
                                    className='btn btn-primary btn-rounded btn-icon' 
                                    type="submit" width='20'>
                        <span className="material-icons-outlined">arrow_forward</span>
                        </button>                
                    <br/>
                    </div>

                </form>
            </div>                    
            
        </div>  
        <div className="row">
            <div id='username-input' className='col'>
                { errorMessage }
            </div>                    
        </div>
       </div> 
    )
}
export default TranslationForm