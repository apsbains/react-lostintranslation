import LoginForm from "../components/Login/LoginForm"

const Login = () => {

    return(
       <div class='container' id='login'>
            
                    <div className="row align-items-center">
                        <div className="col">
                            <img src="./Logo.png" alt="" width="200"/>
                        </div>
                        <div className="col">
                            <h1>Lost in translation</h1>
                            <h4>Get started</h4>
                        </div>                
                    </div>
                    
                    <LoginForm />    
                    
        </div>
    )
}
export default Login