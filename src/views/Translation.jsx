import { translationAdd } from "../api/translation"
import withAuth from "../components/hoc/withAuth"
import TranslationForm from "../components/Translation/TranslationForm"
import TranslationImages from "../components/Translation/TranslationImages"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useUser } from "../context/UserContext"
import { storageSave } from "../utils/storage"
import { useState } from 'react'

let imagesToDisplay = 0;

const Translation = () => {
    //imagesToDisplay = 0;
    //window.location.reload()
    const { user, setUser } = useUser()
    const [ loading, setLoading ] = useState(false)
    
    const handleTranslationClick = async (notes) => {
       
        //dele userimnp
                
        const translationNotes = notes.replace(/[^a-zA-Z]/g, "");
        
        const availableImages = [...translationNotes]
        
        
            imagesToDisplay = availableImages.map((image, index) => {
                return <TranslationImages image={ image } key={ index } />
            })          
        setLoading(true)
        
        //send the HTTP request to the 
        const  [error, updatedUser] = await translationAdd(user, translationNotes) 
        
        if(error !== null ) {
            return
        }

        
        storageSave( STORAGE_KEY_USER, updatedUser )
        setUser(updatedUser)
      
        
    } 
    //const available images = imageIcons.map(item => )
    return(
        <div className='container'>
            <br/>
            <br/>
            <br/>

            <div className='row' id='login-container'>
                <TranslationForm onTranslation={ handleTranslationClick }/>                
            </div>
            <br/>
            <br/>
            <br/>
            <br/>
            <div className='container' id='login-container'>
                
                { loading && imagesToDisplay!==0 && imagesToDisplay  
                }
                
                { loading && imagesToDisplay!==0 &&
                    <div className='row'>
                        <span id='purple'>translation</span>
                    </div>
                }
            </div>
            

        </div>
    )
}
export default withAuth( Translation)