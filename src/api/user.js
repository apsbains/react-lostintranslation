
import { createHeaders } from './index'

const apiUrl = process.env.REACT_APP_API_URL

const checkForUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}?username=${username}`)

        if(!response.ok){
            throw new Error('Could not complete request.')
        }
        const data = await response.json()
        return [null, data]


    } catch(error) {
        return [error.message, []]
    }


}

const createUser = async (username) => {
    
    try {
        const response = await fetch(apiUrl, {
            method: 'POST',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        })

        if(!response.ok){
            throw new Error('Create user with username.' + username)
        }
        const data = await response.json()
        return [null, data]


    } catch(error) {
        return [error.message, []]
    }
}

/*export const addTranslationsToUser = async (user) => {
    
    fetch(`${apiURL}/translations/${userId}`, {
        method: 'PATCH', // NB: Set method to PATCH
        headers: {
            'X-API-Key': apiKey,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            // Provide new translations to add to user with id 1
            translations: ['easy', 'i love javascript'] 
        })
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Could not update translations history')
      }
      return response.json()
    })
    .then(updatedUser => {
      // updatedUser is the user with the Patched data
    })
    .catch(error => {
    })

}*/


export const loginUser = async (username) => {
    const [checkError, user] = await checkForUser(username)

    if(checkError !== null ) {
        return [checkError, null]
    }

    if(user.length > 0) {        
        console.log('Has hit the user with username')
        return [null, user.pop() ]
    }

    console.log('Creating user again do not need to actually')

    return await createUser(username)
}

export const userById = async (userId) => {

    try {
        const response = await fetch(`${apiUrl}/${userId}`)
        if(!response) {
            throw new Error('Could not fetch user')
        }
        const user = await response.json()
        return [ null, user ]

    } catch (error) {
        return [ error.message, null ]
    }
}

